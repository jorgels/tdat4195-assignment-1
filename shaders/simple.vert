#version 430 core

layout(location = 0) in vec3 position;
layout(location = 1) in vec4 vertex_color;
out vec4 fragment_color;

uniform mat4 t_matrix;

void main() {
  gl_Position = t_matrix * vec4(position, 1.0f);
  fragment_color = vertex_color;
}