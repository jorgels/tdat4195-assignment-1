extern crate nalgebra_glm as glm;
use gl::types::*;
use std::sync::{Arc, Mutex, RwLock};
use std::thread;
use std::{mem, os::raw::c_void, ptr};

mod shader;
mod util;

use glutin::event::{
    ElementState::{Pressed, Released},
    Event, KeyboardInput,
    VirtualKeyCode::{self, *},
    WindowEvent,
};
use glutin::event_loop::ControlFlow;

const SCREEN_W: u32 = 800;
const SCREEN_H: u32 = 600;

// Helper functions to make interacting with OpenGL a little bit prettier. You will need these!
// The names should be pretty self explanatory
fn byte_size_of_array<T>(val: &[T]) -> isize {
    std::mem::size_of_val(&val[..]) as isize
}

// Get the OpenGL-compatible pointer to an arbitrary array of numbers
fn pointer_to_array<T>(val: &[T]) -> *const c_void {
    &val[0] as *const T as *const c_void
}

// Get the size of the given type in bytes
fn size_of<T>() -> i32 {
    mem::size_of::<T>() as i32
}

// Get an offset in bytes for n units of type T
// fn offset<T>(n: u32) -> *const c_void {
//     (n * mem::size_of::<T>() as u32) as *const T as *const c_void
// }

fn get_triangle_vao_id(
    vertices: &Vec<GLfloat>,
    indices: &Vec<GLuint>,
    colors: &Vec<GLfloat>,
) -> GLuint {
    let mut vao_id: GLuint = 0;
    let mut vao_buffer_id: GLuint = 0;
    let attrib_index: GLuint = 0;
    unsafe {
        let buffer_size = byte_size_of_array(&vertices);
        let array_ptr = pointer_to_array(&vertices);
        gl::GenVertexArrays(1, &mut vao_id);
        gl::GenBuffers(1, &mut vao_buffer_id);
        gl::BindBuffer(gl::ARRAY_BUFFER, vao_buffer_id);
        gl::BindVertexArray(vao_id);
        gl::BufferData(gl::ARRAY_BUFFER, buffer_size, array_ptr, gl::STATIC_DRAW);
        gl::VertexAttribPointer(
            attrib_index,
            3,
            gl::FLOAT,
            gl::FALSE,
            3 * size_of::<GLfloat>(),
            std::ptr::null(),
        );
        gl::EnableVertexAttribArray(attrib_index);
    }
    let mut color_buffer_id: GLuint = 1;
    let color_attrib_index: GLuint = 1;
    unsafe {
        let buffer_size = byte_size_of_array(&colors);
        let array_ptr = pointer_to_array(&colors);
        gl::GenBuffers(1, &mut color_buffer_id);
        gl::BindBuffer(gl::ARRAY_BUFFER, color_buffer_id);
        gl::BufferData(gl::ARRAY_BUFFER, buffer_size, array_ptr, gl::STATIC_DRAW);
        gl::VertexAttribPointer(
            color_attrib_index,
            4,
            gl::FLOAT,
            gl::FALSE,
            4 * size_of::<GLfloat>(),
            std::ptr::null(),
        );
        gl::EnableVertexAttribArray(color_attrib_index);
    }
    let mut index_buffer_id: GLuint = 2;
    unsafe {
        let buffer_size = byte_size_of_array(&indices);
        let array_ptr = pointer_to_array(&indices);
        gl::GenBuffers(1, &mut index_buffer_id);
        gl::BindBuffer(gl::ELEMENT_ARRAY_BUFFER, index_buffer_id);
        gl::BufferData(
            gl::ELEMENT_ARRAY_BUFFER,
            buffer_size,
            array_ptr,
            gl::STATIC_DRAW,
        );
    }
    return vao_id;
}

struct Camera {
    x: f32,
    y: f32,
    z: f32,
    x_angle: f32,
    y_angle: f32,
}

fn main() {
    // Set up the necessary objects to deal with windows and event handling
    let el = glutin::event_loop::EventLoop::new();
    let wb = glutin::window::WindowBuilder::new()
        .with_title("Gloom-rs")
        .with_resizable(false)
        .with_inner_size(glutin::dpi::LogicalSize::new(SCREEN_W, SCREEN_H));
    let cb = glutin::ContextBuilder::new().with_vsync(true);
    let windowed_context = cb.build_windowed(wb, &el).unwrap();

    // Set up a shared vector for keeping track of currently pressed keys
    let arc_pressed_keys = Arc::new(Mutex::new(Vec::<VirtualKeyCode>::with_capacity(10)));
    // Send a copy of this vector to send to the render thread
    let pressed_keys = Arc::clone(&arc_pressed_keys);

    // Spawn a separate thread for rendering, so event handling doesn't block rendering
    let render_thread = thread::spawn(move || {
        // Acquire the OpenGL Context and load the function pointers. This has to be done inside of the renderin thread, because
        // an active OpenGL context cannot safely traverse a thread boundary
        let context = unsafe {
            let c = windowed_context.make_current().unwrap();
            gl::load_with(|symbol| c.get_proc_address(symbol) as *const _);
            c
        };

        // Set up openGL
        unsafe {
            gl::Enable(gl::CULL_FACE);
            gl::Disable(gl::MULTISAMPLE);
            gl::Enable(gl::BLEND);
            gl::BlendFunc(gl::SRC_ALPHA, gl::ONE_MINUS_SRC_ALPHA);
            gl::Enable(gl::DEBUG_OUTPUT_SYNCHRONOUS);
            gl::DebugMessageCallback(Some(util::debug_callback), ptr::null());
        }

        let vertices_triforce: Vec<GLfloat> = vec![
            -0.25, -0.25, 0.0, 0.25, -0.25, 0.0, 0.0, 0.25, 0.0, 0.75, -0.25, 0.0, 0.5, 0.25, 0.0,
            0.25, 0.75, 0.0,
        ];

        let indices_triforce: Vec<GLuint> = vec![0, 1, 2, 1, 3, 4, 2, 4, 5];
        let colors: Vec<GLfloat> = vec![
            1.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0, 1.0, 0.0, 0.0, 1.0, 1.0, 0.0, 1.0, 1.0, 1.0, 1.0,
            0.0, 1.0, 1.0, 1.0, 1.0, 1.0, 1.0,
        ];

        let shader: shader::Shader;

        unsafe {
            shader = shader::ShaderBuilder::new()
                .attach_file("/home/zalox/src/gloom-rs/shaders/simple.frag")
                .attach_file("/home/zalox/src/gloom-rs/shaders/simple.vert")
                .link();
        }

        let _vao_id = get_triangle_vao_id(&vertices_triforce, &indices_triforce, &colors);

        // Used to demonstrate keyboard handling -- feel free to remove
        let mut _arbitrary_number = 0.0;

        let first_frame_time = std::time::Instant::now();
        let mut last_frame_time = first_frame_time;

        let name = std::ffi::CString::new("t_matrix").unwrap();
        let t_matrix_location_name = name.into_raw(); //this leaks the string, but nowhere to clear it as the loop has no exit

        let mut ratio = SCREEN_H as f32;
        ratio = ratio / (SCREEN_W as f32);

        let mut camera = Camera {
            x: 0.0,
            y: 0.0,
            z: -2.0,
            x_angle: 0.0,
            y_angle: 0.0,
        };

        let abc_location =
            unsafe { gl::GetUniformLocation(shader.program_id, t_matrix_location_name) };

        let _perspective: glm::Mat4;

        loop {
            let now = std::time::Instant::now();
            // let elapsed = now.duration_since(first_frame_time).as_secs_f32();
            let delta_time = now.duration_since(last_frame_time).as_secs_f32();
            last_frame_time = now;

            // Handle keyboard input
            if let Ok(keys) = pressed_keys.lock() {
                for key in keys.iter() {
                    match key {
                        VirtualKeyCode::A => {
                            camera.x += delta_time;
                        }
                        VirtualKeyCode::D => {
                            camera.x -= delta_time;
                        }
                        VirtualKeyCode::W => {
                            camera.z += delta_time;
                        }
                        VirtualKeyCode::S => {
                            camera.z -= delta_time;
                        }
                        VirtualKeyCode::E => {
                            camera.y -= delta_time;
                        }
                        VirtualKeyCode::Q => {
                            camera.y += delta_time;
                        }
                        VirtualKeyCode::Down => {
                            camera.x_angle -= delta_time;
                        }
                        VirtualKeyCode::Up => {
                            camera.x_angle += delta_time;
                        }
                        VirtualKeyCode::Right => {
                            camera.y_angle -= delta_time;
                        }
                        VirtualKeyCode::Left => {
                            camera.y_angle += delta_time;
                        }

                        _ => {}
                    }
                }
            }

            let translate: glm::Mat4 =
                glm::translate(&glm::identity(), &glm::vec3(camera.x, camera.y, camera.z));
            let rotate_y = glm::rotate(&glm::identity(), camera.y_angle, &glm::vec3(0.0, 1.0, 0.0));
            let rotate_x = glm::rotate(&glm::identity(), camera.x_angle, &glm::vec3(1.0, 0.0, 0.0));

            let view_matrix =
                glm::perspective(ratio, 45.0, 1.0, 100.0) * rotate_x * rotate_y * translate;

            unsafe {
                gl::ClearColor(0.163, 0.163, 0.163, 1.0);
                gl::Clear(gl::COLOR_BUFFER_BIT);
                gl::UseProgram(shader.program_id);
                gl::UniformMatrix4fv(abc_location, 1, 0, view_matrix.as_ptr());
                gl::DrawElements(gl::TRIANGLES, 9, gl::UNSIGNED_INT, std::ptr::null());
            }

            context.swap_buffers().unwrap();
        }
    });

    // Keep track of the health of the rendering thread
    let render_thread_healthy = Arc::new(RwLock::new(true));
    let render_thread_watchdog = Arc::clone(&render_thread_healthy);
    thread::spawn(move || {
        if !render_thread.join().is_ok() {
            if let Ok(mut health) = render_thread_watchdog.write() {
                println!("Render thread panicked!");
                *health = false;
            }
        }
    });

    // Start the event loop -- This is where window events get handled
    el.run(move |event, _, control_flow| {
        *control_flow = ControlFlow::Wait;

        // Terminate program if render thread panics
        if let Ok(health) = render_thread_healthy.read() {
            if *health == false {
                *control_flow = ControlFlow::Exit;
            }
        }

        match event {
            Event::WindowEvent {
                event: WindowEvent::CloseRequested,
                ..
            } => {
                *control_flow = ControlFlow::Exit;
            }
            // Keep track of currently pressed keys to send to the rendering thread
            Event::WindowEvent {
                event:
                    WindowEvent::KeyboardInput {
                        input:
                            KeyboardInput {
                                state: key_state,
                                virtual_keycode: Some(keycode),
                                ..
                            },
                        ..
                    },
                ..
            } => {
                if let Ok(mut keys) = arc_pressed_keys.lock() {
                    match key_state {
                        Released => {
                            if keys.contains(&keycode) {
                                let i = keys.iter().position(|&k| k == keycode).unwrap();
                                keys.remove(i);
                            }
                        }
                        Pressed => {
                            if !keys.contains(&keycode) {
                                keys.push(keycode);
                            }
                        }
                    }
                }

                // Handle escape separately
                match keycode {
                    Escape => {
                        *control_flow = ControlFlow::Exit;
                    }
                    _ => {}
                }
            }
            _ => {}
        }
    });
}
